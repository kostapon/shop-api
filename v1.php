﻿<?php

namespace Artamonov\Api\Controllers;


use Artamonov\Api\Controller;
use Artamonov\Api\Response;
use \CModule;
use \CIBlockSection;
use \CFile;
use \CIBlockElement;
use Bitrix\Highloadblock\HighloadBlockTable;
use \CPrice;
use \CSaleBasket;
use \CSaleUser;
use \CCatalogSKU;
use \CUser;
use \CEvent;
use \CSaleOrder;
use \CSalePaySystemAction;
use \CSaleDelivery;
use \CSaleDeliveryHandler;
use \CSaleOrderProps;
use \CSaleOrderPropsValue;
use \CSaleStatus;
use \CSalePaySystem;
use \CSaleLocation;

class v1 extends Controller
{
     var $HTTP = 'http://';  
     var $HTTPS = 'https://';  
     
    /**
     * Запрос иерархии товаров
     * @return $arResult = array(
     *      result => ok || error,
     *      data   => array(
     *                      array(
     *                          name - название раздела
     *                          id - ID раздела
     *                          parent - ID раздела родителя
     *                          icon - иконка раздела
     *                          image - картинка раздела
     *                          count - количество товаров в разделе
     *                      )
     *                )
     *      )
     * )
     */
    public function catalog()
    {  
        if(CModule::IncludeModule("iblock")){
            $result = array();
            $res = CIBlockSection::GetList(
                array(),
                array(
                    'IBLOCK_ID'  => 4, 
                    'ACTIVE'     => 'Y',
                    'CNT_ACTIVE' => 'Y'
                ), 
                true, 
                array('NAME', 'ID', 'IBLOCK_SECTION_ID', 'PICTURE', 'UF_IMAGE_ICON', 'ELEMENT_CNT'));
            if($res){
                while($ob = $res->GetNextElement())
                {
                    $arFields = $ob->GetFields();
                    if($arFields['PICTURE']){
                        $arFields['PICTURE'] = $this->HTTP.$_SERVER['SERVER_NAME'].CFile::GetPath($arFields['PICTURE']);
                    }
                    if($arFields['UF_IMAGE_ICON']){
                        $arFields['UF_IMAGE_ICON'] = $this->HTTP.$_SERVER['SERVER_NAME'].CFile::GetPath($arFields['UF_IMAGE_ICON']);
                    }
                    $data=array(
                        'name' => $arFields['NAME'],
                        'id' => $arFields['ID'],
                        'parent' => $arFields['IBLOCK_SECTION_ID'],
                        'icon' => $arFields['PICTURE'],
                        'image' => $arFields['UF_IMAGE_ICON'],
                        'count' => $arFields['ELEMENT_CNT']
                    );
                    $result[] = $data;
                }
                $arResult = array(
                    'result' => 'ok',
                    'data' => $result
                ); 
            }else{
                $arResult = array(
                    'result' => 'error'
                );
            }
            
    
            Response::ShowResult($arResult);
        }
    }
    
    /**
     * Баннеры слайдера
     * @return $arResult = array(
     *              result => ok || error,     
     *              data => array(
     *                  array(
     *                      title - название слайда
     *                      text - описание для слайда
     *                      image - изображение
     *                  )
     *              )
     * )
     */
    public function banners(){
        if(CModule::IncludeModule("iblock")){
            $res = CIBlockElement::GetList(
                array(
                    'sort' => 'asc'
                ),
                array(
                    'IBLOCK_ID'  => 3, 
                    'ACTIVE'     => 'Y'
                ),
                false,
                false,
                array(
                    'NAME',
                    'DETAIL_TEXT',
                    'PREVIEW_PICTURE',
                    'DETAIL_PICTURE'
                ));
            if($res){
                while($ob = $res->GetNextElement()){
                    $arFields = $ob->GetFields();
                    if($arFields['PREVIEW_PICTURE']){
                        $arFields['PREVIEW_PICTURE'] = $this->HTTP.$_SERVER['SERVER_NAME'].CFile::GetPath($arFields['PREVIEW_PICTURE']);
                    }
                    if($arFields['DETAIL_PICTURE']){
                        $arFields['DETAIL_PICTURE'] = $this->HTTP.$_SERVER['SERVER_NAME'].CFile::GetPath($arFields['DETAIL_PICTURE']);
                    }
                    $data = array(
                        'title' => $arFields['NAME'],
                        'text' => strip_tags($arFields['DETAIL_TEXT']),
                        'image' => $arFields['DETAIL_PICTURE']!=null ? $arFields['DETAIL_PICTURE'] : $arFields['PREVIEW_PICTURE']
                    );
                    $result[] = $data;
                }
                $arResult = array(
                    'result' => 'ok',
                    'data' => $result
                ); 
            }else{
                $arResult = array(
                    'result' => 'error'
                );
            }
            
            Response::ShowResult($arResult);
        }
    }
    
    /**
     * Все товары категории
     * @param
     *      $user - ID пользователя
     *      $section - ID раздела каталога
     *      //ДЛЯ ФИЛЬТРАЦИИ
     *      $price1 - начальная цена
     *      $price2 - конечная цена
     *      $colors - массив цветов
     *      $size - массив размеров одежны
     *      $shoesizes - массив размеров обуви
     * @return 
     *      $arResult = array(
     *          result => ok || error,
     *          data => array(
     *              array(
     *                  image - изображение товара
     *                  name - название товара
     *                  price - стоимость
     *                  id - ID товара
     *                  date - дата изменения товара
     *                  count_show - количество просмотров данного товара
     *              )
     *          )
     *      )
     */
    public function section(){
        if(CModule::IncludeModule("iblock")){
            $json = file_get_contents('php://input');
            if( isset($json) ){
                $data=$json;
                $data=json_decode($data);
                $user = htmlspecialchars($data->user);
                $section = htmlspecialchars($data->section);
                
                $price1 = htmlspecialchars($data->price1);
                $price2 = htmlspecialchars($data->price2);
                $colors = ($data->colors);
                $size = ($data->size);
                $shoesizes = ($data->shoesizes);
                
                if($section!=0){
                    $arFilter = array(
                        'IBLOCK_ID'  => 4, 
                        'ACTIVE'     => 'Y',
                        'SECTION_ID' => array($section),
                        'INCLUDE_SUBSECTIONS' => 'Y'
                    );
                }else{
                    $arFilter = array(
                        'IBLOCK_ID'  => 4, 
                        'ACTIVE'     => 'Y'
                    );
                }
                
                if(!empty($price1)||!empty($price2)||!empty($colors)||!empty($size)||!empty($shoesizes)){
                    
                    
                    if(CModule::IncludeModule("highloadblock")){
                                        $hlblock = HighloadBlockTable::getById(3)->fetch();   
                                        $entity = HighloadBlockTable::compileEntity($hlblock);
                                        $entity_data_class = $entity->getDataClass();
                                        $rsData = $entity_data_class::getList(array(
                                           'select' => array('UF_XML_ID','UF_RGB','UF_NAME')
                                        ));
                                        $arColor=array();
                                        while($el = $rsData->fetch()){
                                            foreach($colors as $itcolor){
                                                if($el['UF_NAME']==$itcolor)
                                                    $arColor[] = $el['UF_XML_ID'];
                                            }
                                        }
                                   }
                    
                    
                    $arFilterfilter = array('IBLOCK_ID' => 5,
                                            'ACTIVE' => 'Y',
                                            'PROPERTY_COLOR_REF' => $arColor,
                                            'PROPERTY_SIZES_CLOTHES_VALUE' => $size,
                                            'PROPERTY_SIZES_SHOES_VALUE' => $shoesizes
                                            );
                    $filter = CIBlockElement::GetList(array(),
                                                      $arFilterfilter,
                                                      false,
                                                      false,
                                                      array('ID','PROPERTY_CML2_LINK','PROPERTY_SIZES_SHOES'));
                    $obfil = array();
                    
                    while($fil = $filter->GetNextElement()){
                        $arfil = $fil->GetFields();
                        $obfil[] = $arfil['PROPERTY_CML2_LINK_VALUE'];
                    }
                    
                    $arFilter['ID']=$obfil;
                    
                    if(!empty($price2)){
                        $arFilter['<=PROPERTY_MAXIMUM_PRICE'] = $price2;
                    }
                    if(!empty($price1)){
                        $arFilter['>=PROPERTY_MAXIMUM_PRICE'] = $price1;
                    }
                }
                
                
                $res = CIBlockElement::GetList(
                    array(),
                    $arFilter,
                    false,
                    false,
                    array(
                        'PREVIEW_PICTURE',
                        'NAME',
                        'ID',
                        'PROPERTY_MAXIMUM_PRICE',
                        'TIMESTAMP_X',
                        'SHOW_COUNTER'
                    ));
                    if($res){
                        while($ob = $res->GetNextElement()){
                            $arFields = $ob->GetFields();
                            if($arFields['PREVIEW_PICTURE']){
                                $arFields['PREVIEW_PICTURE'] = $this->HTTP.$_SERVER['SERVER_NAME'].CFile::GetPath($arFields['PREVIEW_PICTURE']);
                            }
                            $name = str_replace('&quot;','',$arFields['NAME']);
                            $data = array(
                                'image' => $arFields['PREVIEW_PICTURE'],
                                'name' => $name,
                                'price' => $arFields['PROPERTY_MAXIMUM_PRICE_VALUE'],
                                'id' => $arFields['ID'],
                                'date' => $arFields['TIMESTAMP_X'],
                                'count_show' => $arFields['SHOW_COUNTER']
                            );
                            $result[] = $data;
                        }
                        $arResult = array(
                            'result' => 'ok',
                            'data' => $result==null ? '' : $result
                        );
                    }else{
                        $arResult = array(
                            'result' => 'error'
                        );
                    }
            }else{
                $arResult = array(
                    'result' => 'incorrect'
                );
            }
            
            Response::ShowResult($arResult);
        }
    }
    
    /**
     * ВЫВОД ДИАПАЗОНА ПАРАМЕТРОВ ДЛЯ ФИЛЬТРА
     * @param 
     *      $section - id раздела
     * @return
     *      $arResult = array(
     *          result => ok || error,
     *          data => array(
     *              colors - массив цветов товаров
     *              size - массив размеров одежды
     *              shorsize - массив размеров обуви
     *              minprice - минимальная цена среди товаров
     *              maxprice - максимальная цена среди
     *          )
     *      )
     */
    function filter_prop(){
        
        $json = file_get_contents("php://input");
                
        if( isset($json) ){
            $data=$json;
            $data=json_decode($data);
            $section = htmlspecialchars($data->section);
            
            if(!empty($section) || (empty($section) && is_numeric($section)) ){
                
                if($section!=0){
                    $arFilter = array(
                        'IBLOCK_ID'  => 4, 
                        'ACTIVE'     => 'Y',
                        'SECTION_ID' => array($section),
                        'INCLUDE_SUBSECTIONS' => 'Y'
                    );
                }else{
                    $arFilter = array(
                        'IBLOCK_ID'  => 4, 
                        'ACTIVE'     => 'Y'
                    );
                }
                
                if(CModule::IncludeModule("iblock")){
                    
                    $elements = CIBlockElement::GetList(array(),$arFilter,false,false,array('ID','PROPERTY_MAXIMUM_PRICE'));
                    $elem = array();
                    $pricemin = 0;
                    $pricemax = 0;
                    $i=0;
                    while($element = $elements->GetNextElement()){
                        $arelem = $element->GetFields();
                        $elem[] = $arelem['ID'];
                        if($i==0){
                            $pricemin = round($arelem['PROPERTY_MAXIMUM_PRICE_VALUE']);
                            $pricemax = round($arelem['PROPERTY_MAXIMUM_PRICE_VALUE']);
                        }
                        $i++;
                        if($pricemin > round($arelem['PROPERTY_MAXIMUM_PRICE_VALUE'])) $pricemin = round($arelem['PROPERTY_MAXIMUM_PRICE_VALUE']);
                        if($pricemax < round($arelem['PROPERTY_MAXIMUM_PRICE_VALUE'])) $pricemax = round($arelem['PROPERTY_MAXIMUM_PRICE_VALUE']);
                    }
                    $colors = CIBlockElement::GetList(array(),
                                                      array(
                                                        'IBLOCK_ID' => 5, 
                                                        'ACTIVE' => 'Y', 
                                                        'PROPERTY_CML2_LINK' => $elem 
                                                      ),
                                                      false,
                                                      false,
                                                      array(
                                                        'PROPERTY_COLOR_REF',
                                                        'PROPERTY_SIZES_SHOES',
                                                        'PROPERTY_SIZES_CLOTHES'
                                                        ));
                    $arcolor = array();
                    $arsize = array();
                    $arshorsize = array();
                    while($itemcolor = $colors->GetNextElement()){
                        $arelemcol = $itemcolor->GetFields();
                        $arcolor[] = $arelemcol['PROPERTY_COLOR_REF_VALUE'];
                        $arsize[] = $arelemcol['PROPERTY_SIZES_CLOTHES_VALUE'];
                        $arshorsize[] = $arelemcol['PROPERTY_SIZES_SHOES_VALUE'];
                    }
                    
                    $arcolor = array_unique($arcolor);
                    $arsize = array_unique($arsize); 
                    $arshorsize = array_unique($arshorsize);
                    
                    $color = array();
                    foreach($arcolor as $item){
                        if($item!=null)
                            $color[]=$item;
                    }
                    
                    if(CModule::IncludeModule("highloadblock")){
                                        $hlblock = HighloadBlockTable::getById(3)->fetch();   
                                        $entity = HighloadBlockTable::compileEntity($hlblock);
                                        $entity_data_class = $entity->getDataClass();
                                        $rsData = $entity_data_class::getList(array(
                                           'select' => array('UF_XML_ID','UF_RGB','UF_NAME')
                                        ));
                                        $arColor=array();
                                        while($el = $rsData->fetch()){
                                            foreach($color as $itcolor){
                                                if($el['UF_XML_ID']==$itcolor)
                                                    $arColor[] = array('name' => $el['UF_NAME'], 'rgb' => $el['UF_RGB']);
                                            }
                                        }
                                   }
                    $color = $arColor;
                    
                    $size = array();
                    foreach($arsize as $item){
                        if($item!=null)
                            $size[]=$item;
                    }
                    $arsize = $size;
                    $size = array();
                    $arsize2 = array();
                    $arsize3 = array();
                    foreach($arsize as $item){
                        switch ($item){
                            case '1г.':
                                $size[]=$item;
                                break;
                            case '2г.':
                                $size[]=$item;
                                break;
                            case '3г.':
                                $size[]=$item;
                                break;
                            case '4г.':
                                $size[]=$item;
                                break;
                            case '5л.':
                                $size[]=$item;
                                break;
                            case '6л.':
                                $size[]=$item;
                                break;
                            case '7л.':
                                $size[]=$item;
                                break;
                            default:
                                $arsize2[]=$item;
                        }
                    }
                    sort($size);
                    
                    $size2 = array();
                    foreach($arsize2 as $item){
                        switch ($item){
                            case '40':
                                $size2[]=$item;
                                break;
                            case '42':
                                $size2[]=$item;
                                break;
                            case '44':
                                $size2[]=$item;
                                break;  
                            case '46':
                                $size2[]=$item;
                                break;
                            case '48':
                                $size2[]=$item;
                                break;  
                            case '64':
                                $size2[]=$item;
                                break;
                            case '68':
                                $size2[]=$item;
                                break;
                            case '72':
                                $size2[]=$item;
                                break;
                            default:
                                $arsize3[]=$item;
                        }
                    }
                    sort($size2);
                    
                    $size = array_merge($size,$size2);
                    $size = array_merge($size,$arsize3);
                    
                    $shorsize = array();
                    foreach($arshorsize as $item){
                        if($item!=null)
                            $shorsize[]=$item;
                    }
                    sort($shorsize);
                    
                    $arResult = array(
                        'result' => 'ok',
                        'data' => array( 'colors' => $color,
                                         'size' => $size,
                                         'shorsize' => $shorsize,
                                         'minprice' => $pricemin,
                                         'maxprice' => $pricemax )
                    );
                    
                }else{
                    $arResult = array(
                        'result' => 'error',
                        'error' => 'No connection iblock'
                    );
                }
            }else{
                $arResult = array(
                    'result' => 'incorrect',
                    'error' => 'No section'
                );
            }
        }else{
            $arResult = array(
                    'result' => 'incorrect'
                );
        }
        
        Response::ShowResult($arResult);
    }
    
    
    /**
     * Продукт
     * @param
     *      $uses - ID пользователя
     *      $product - ID товара
     * @return
     *      $arResult = array(
     *          result => ok || error,
     *          data => array(
     *              max - максимальное количество единиц товара, которых разрешено добавить в корзину
     *              name - название товара
     *              color - массив возможных цветов товара
     *              shoesizes - массив возможных размеров обуви у данного товара
     *              sizes - массив возможных размеров одежды у данного товара
     *              property - массив доп. свойств товара (array(array('name'=>'название свойства', 'value'=>'значение свойства')))
     *              image - основное изображение товара
     *              fullprice - цена со скидкой
                    price - цена товара
     *          )
     *      )
     */
    function product(){
        if(CModule::IncludeModule("iblock")){
            $json = file_get_contents('php://input');
            if( isset($json) ){
                $data=json_decode($json);
                $user=htmlspecialchars($data->user);
                $product=htmlspecialchars($data->product);
                $res = CIBlockElement::GetProperty(4,$product,array(),array('CODE'=>'MAXIMUM_PURCHASE'));//получение макс кол покуп
                    if($res){
                        while($ob = $res->GetNext()){
                            if($ob['VALUE']==null){
                               $ob['VALUE']='10'; 
                            }
                            $max=$ob['VALUE'];
                        }
                        $result['max'] = $max;
                        $dat = CIBlockElement::GetByID($product);
                        $ob = $dat->GetNext();
                        $result['name'] = str_replace('&quot;','',$ob['NAME']);
                        if($result['name']){
                     
                       
                        $res = CIBlockElement::GetList(//получение торг предлож
                            array(),
                            array(
                                'IBLOCK_ID'                => 5, 
                                'ACTIVE'                   => 'Y',
                                'PROPERTY_CML2_LINK' => $product
                            ),
                            false,
                            false,
                            array('ID')
                        );
                       while($ob = $res->GetNextElement()){
                            $arFields = $ob->GetFields();
                            
                            $ress = CIBlockElement::GetProperty(5,$arFields['ID']);//получение свойств
                            $arProp = array();
                            while($obb = $ress->GetNext()){
                                $arProp[]=array( 'name' => $obb['NAME'], 'value' => $obb['VALUE'] );
                                if($obb['CODE']=='COLOR_REF'){//если это цвет
                                   if(CModule::IncludeModule("highloadblock")){
                                        $hlblock = HighloadBlockTable::getById(3)->fetch();   
                                        $entity = HighloadBlockTable::compileEntity($hlblock);
                                        $entity_data_class = $entity->getDataClass();
                                        $rsData = $entity_data_class::getList(array(
                                           'select' => array('UF_XML_ID','UF_RGB','UF_NAME')
                                        ));
                                        $arColor=array();
                                        while($el = $rsData->fetch()){
                                            if($el['UF_XML_ID']==$obb['VALUE'])
                                                $arColor = array('rgb' => $el['UF_RGB'], 'name' => $el['UF_NAME']);
                                        }
                                   }
                                }
                                if($obb['CODE']=='MORE_PHOTO'){//если это картинка
                                    if(!empty($obb['VALUE'])){
                                        $arColor['image'][]=$this->HTTP.$_SERVER['SERVER_NAME'].CFile::GetPath($obb['VALUE']);
                                    }else{
                                        $dat_image = CIBlockElement::GetByID($product);
                                        $ob_image = $dat_image->GetNextElement();
                                        $arFields_image = $ob_image->GetFields();
                                        if(!empty($arFields_image['DETAIL_PICTURE'])){
                                            $arColor['image'][]=$this->HTTP.$_SERVER['SERVER_NAME'].CFile::GetPath($arFields_image['DETAIL_PICTURE']);
                                        }else{
                                            $arColor['image'][]='';
                                        }
                                    }
                                }
                                if($obb['CODE']=='SIZES_SHOES'){//если размер обуви
                                    $arShoesizes=$obb['VALUE_ENUM'];
                                }
                                if($obb['CODE']=='SIZES_CLOTHES'){//если размер одежды
                                    $arSize=$obb['VALUE_ENUM'];
                                }
                            }
                            $prise=CPrice::GetBasePrice($arFields['ID']);
                            
                            $resultColor[] = $arColor;
                            $arShoesizesus[] = $arShoesizes;
                            $arSizeus[] = $arSize;
                            $arprise = $prise['PRICE'];
                            $fullprice = $prise['PRICE'];
                        }
                        
                        $counttotal = count($resultColor);
                                                
                        //<формирование массива цвета>----------------------------------------------------------------------------------------------
                        $arresultColor = array();
                        $datacol = array();
                        foreach($resultColor as $color){
                            $flag=array_search($color['name'],$datacol);
                            if( !$flag  ){
                                if(!is_numeric($flag)){
                                    $arresultColor[] = $color;
                                    $datacol[] = $color['name'];
                                }
                            }
                        }
                        
                        $arresultColorsus = array();
                        
                        foreach($arresultColor as $color){
                            foreach($resultColor as $itemCol){
                                if($color['name']==$itemCol['name']){
                                    $color['countcolor']++;
                                }
                            }
                            $arresultColorsus[] = $color;
                        }
                        
                        $result['color'] = $arresultColorsus;
                       //</формирование массива цвета>----------------------------------------------------------------------------------------------
                       
                       //<формирование массива размера обуви>------------------------------------------------------------------------------------------
                        $ardatares = array();
                        $arShoesizesar = array();
                        foreach($arShoesizesus as $items){
                            $flag=array_search($items,$ardatares);
                            if( !$flag ){
                                if(!is_numeric($flag)){
                                    $arShoesizesar[]['size']=$items;
                                    $ardatares[]=$items;
                                }
                            }
                        }
                        
                        $arShoesizesarray = array();
                        
                        foreach($arShoesizesar as $itemar){
                            foreach($arShoesizesus as $itemRes){
                                if($itemar['size']==$itemRes){
                                    $itemar['countsize']++;
                                }
                            }
                            $arShoesizesarray[] = $itemar;
                        }
                        
                        if($arShoesizesarray[0]['size']==null){
                            $arShoesizesarray[0]['size']=null;
                            $arShoesizesarray[0]['countsize']=null;
                        }
                        
                        $result['shoesizes'] = $arShoesizesarray;
                        //</формирование массива размера обуви>------------------------------------------------------------------------------------------
                        
                        //<формирование массива размера одежды>---------------------------------------------------------------------------------------------
                        $ardatares = array();
                        $arShoesizesar = array();
                        foreach($arSizeus as $items){
                            $flag=array_search($items,$ardatares);
                            if( !$flag ){
                                if(!is_numeric($flag)){
                                    $arShoesizesar[]['size']=$items;
                                    $ardatares[]=$items;
                                }
                            }
                        }
                        
                        $arShoesizesarray = array();
                        
                        foreach($arShoesizesar as $itemsize){
                            foreach($arSizeus as $itemsi){
                                if($itemsize['size']==$itemsi){
                                    $itemsize['countsize']++;
                                }
                            }
                            $arShoesizesarray[] = $itemsize;
                        }
                        
                        if($arShoesizesarray[0]['size']==null){
                            $arShoesizesarray[0]['size']=null;
                            $arShoesizesarray[0]['countsize']=null;
                        }
                        
                        $result['sizes'] = $arShoesizesarray;
                        //</формирование массива размера одежды>---------------------------------------------------------------------------------------------
                        
                        //<цена и полная цена>---------------------------------------------------------------------------------------------------------------
                        $result['fullprice'] = null;
                        $result['price'] = $fullprice;
                        //</цена и полная цена>---------------------------------------------------------------------------------------------------------------
                        
                        //<получаем свойства товара>-------------------------------------------------------------------------------------------------------
                        $res = CIBlockElement::GetProperty(
                            4,
                            $product
                                );//получение макс кол покуп
                        while($ob = $res->GetNext()){
                            if($ob['CODE']=='ARTNUMBER' || 
                            $ob['CODE']=='BRAND' || 
                            $ob['CODE']=='MATERIAL' || 
                            $ob['CODE']=='YEAR' 
                            ){
                                $result['property'][]=array('name'=>$ob['NAME'],'value'=>strip_tags($ob['VALUE']));
                            }
                            if($ob['CODE']=='COMPOSITION'){
                                $val = $ob['~VALUE']['TEXT'];
                                $val = strip_tags($val);
                                $result['property'][]=array('name'=>$ob['NAME'],'value'=>$val);
                            }
                            if($ob['CODE']=='MANUFACTURER'){
                                $result['property'][]=array('name'=>$ob['NAME'],'value'=>strip_tags($ob['VALUE_ENUM']));
                            }
                        }
                        $dat = CIBlockElement::GetByID($product);
                        $ob = $dat->GetNextElement();
                        $arFields = $ob->GetFields();
                        $result['property'][]=array('name'=>'Детальное описание','value'=>html_entity_decode(strip_tags($arFields['DETAIL_TEXT'])));
                        //</получаем свойства товара>-------------------------------------------------------------------------------------------------------
                        
                        //<получаем общую картинку товара>-------------------------------------------------------------------------------------------------------
                        if($arFields['DETAIL_PICTURE']){
                            $result['image'] = $this->HTTP.$_SERVER['SERVER_NAME'].CFile::GetPath($arFields['DETAIL_PICTURE']);
                        }else{
                            $result['image'] = null;
                        }
                        //</получаем общую картинку товара>-------------------------------------------------------------------------------------------------------
                        
                        if($result['price']==null){
                            $prise = CPrice::GetBasePrice($arFields['ID'])['PRICE'];
                            $result['fullprice'] = null;
                            $result['price'] = $prise;
                        }
                        
                        $arResult = array(
                            'result' => 'ok',
                            'data' => $result==null ? '' : $result
                        );
                        }
                        else{
                            $arResult = array(
                                'result' => 'error'
                            );
                        }
                    }else{
                        $arResult = array(
                            'result' => 'error'
                        );
                    }
            }else{
                $arResult = array(
                    'result' => 'incorrect'
                );
            }
            
            Response::ShowResult($arResult);
        }
    }
    
      
    /**
     * ПОЛУЧИТЬ КОЛИЧЕСТВО ТОВАРОВ В КОРЗИНЕ
     * @param
     *      $user - ID пользователя
     *      $token - токен пользователя
     * @return
     *      $arResult = array(
     *          result => ok || error,
     *          data => $i - количество товаров в корзине
     *      )
     */
    function backet_count(){
        if(CModule::IncludeModule("sale")){
            $json = file_get_contents('php://input');
            if( isset($json) ){
                $data=json_decode($json);
                $user=htmlspecialchars($data->user);
                
                $token=htmlspecialchars($data->token);
                if ($this->autorize_token($user,$token)){
                    
                if(!empty($user)){
                    
                    $arFUser = CSaleUser::GetList(array('USER_ID' => $user));//получаем код пользователя (для корзины))
                    if(!$arFUser['ID']){//если нет кода для данного пользователя
                         $int = CSaleUser::_Add(array('USER_ID' => $user));
                    }
                    
                    if($arFUser = CSaleUser::GetList(array('USER_ID' => $user))){//получаем код пользователя (для корзины))
                    $dataBasket = CSaleBasket::GetList(
                                                array(),
                                                array('FUSER_ID' => $arFUser, 'ORDER_ID' => NULL, 'CAN_BUY' => 'Y', 'DELAY' => 'N'),
                                                false,
                                                false,
                                                array(
                                                    'ID'
                                                    ));//получаем товары, которые не оплачены
                    $i=0;
                    while($ob = $dataBasket->Fetch()){
                        $i++;
                    }
                    
                    $arResult = array('result' => 'ok',
                                    'data' => $i);
                                    
                }else{
                    $arResult = array('result' => 'incorrect',
                                    'error' => 'Could not get user code');
                }
                }else{
                    $arResult = array('result' => 'incorrect',
                                    'error' => 'No user parameter specified');
                }
            }else{
                $arResult = array(
                    'result' => 'incorrect',
                    'error' => 'Invalid token'
                );
            }
            }else{
                $arResult = array('result' => 'error',
                                    'error' => 'Could not read data');
            }
        }else{
            $arResult = array('result' => 'error',
                                'error' => 'Module power failure sale');
        }
        
        Response::ShowResult($arResult);
    }
    
    /**
     * РЕДАКТИРОВАНИЕ, УДАЛЕНИЕ ТОВАРА ИЗ КОРЗИНЫ
     * @param
     *      $uses - ID пользователя  
     *      $token - токен пользователя
     *      $product - ID продукта
     *      $color - название цвета
     *      $size - размер одежды
     *      $shoesizes - размер обуви
     *      $count - количество товара
     *      $idasket - ID продукта в корзине
     *      $delay - Отложить товар (true - отложить, false - нет)
     * @return
     *      $arResult = array(
     *          result => ok || error
     *          data - описание ошибки (если произошла)
     *      )
     */
    function updatebasket(){
        if(CModule::IncludeModule("sale")){
            $json = file_get_contents('php://input');
            if( isset($json) ){
                $data=json_decode($json);
                $user=htmlspecialchars($data->user);
                $product=htmlspecialchars($data->product);
                $color=htmlspecialchars($data->color);
                $size=htmlspecialchars($data->size);
                $shoesizes=htmlspecialchars($data->shoesizes);
                $count=htmlspecialchars($data->count);
                $idasket=htmlspecialchars($data->idasket);
                $delay=htmlspecialchars($data->delay);
                
                $token=htmlspecialchars($data->token);
                if ($this->autorize_token($user,$token)){
                
                //получаем значение символьного кода переданного цвета
                if(CModule::IncludeModule("highloadblock")){
                    $hlblock = HighloadBlockTable::getById(3)->fetch();   
                    $entity = HighloadBlockTable::compileEntity($hlblock);
                    $entity_data_class = $entity->getDataClass();
                    $rsData = $entity_data_class::getList(array(
                        'select' => array('UF_XML_ID','UF_RGB','UF_NAME')
                    ));
                    while($el = $rsData->fetch()){
                        if($el['UF_NAME']==$color)
                            $arColor = $el['UF_XML_ID'];//получили значение
                    }
                                        
                //получаем id торгового предложения с переданноми свойствами
                if(CModule::IncludeModule("iblock")){
                    $res = CIBlockElement::GetList(
                        array(),
                        array(
                            'IBLOCK_ID'                => 5, 
                            'ACTIVE'                   => 'Y',
                            'PROPERTY_CML2_LINK' => $product,
                            'PROPERTY_COLOR_REF' => $arColor,
                            'PROPERTY_SIZES_SHOES_VALUE' => $shoesizes,
                            'PROPERTY_SIZES_CLOTHES_VALUE' => $size
                        ),
                        false,
                        false,
                        array('ID'));
           
                    $ob = $res->GetNextElement();
                    if($ob){
                        $arFields = $ob->GetFields();
                        $idpreplog = $arFields['ID'];//ID торгового предложения для данного товара с переданными свойствами
                    }else{
                        $idpreplog = null;
                    }
                    
                     $Props = array();
                     if(!empty($color)) $Props[] = array( 'CODE' => 'COLOR_REF' , 'VALUE' => $color );
                     if(!empty($size)) $Props[] = array( 'CODE' => 'SIZES_CLOTHES' , 'VALUE' => $size );
                     if(!empty($shoesizes)) $Props[] = array( 'CODE' => 'SIZES_SHOES' , 'VALUE' => $shoesizes );
                     
                     if(!empty($idpreplog)){
                        $arData = array('QUANTITY' => $count, 'PRODUCT_ID' => $idpreplog, 'PROPS' => $Props, 'PRODUCT_XML_ID' => $product.'#'.$idpreplog );
                     }else{
                        $arData = array('QUANTITY' => $count, 'PROPS' => $Props);
                     } 
                     
                     if($delay){$arData['DELAY']='Y';}else{$arData['DELAY']='N';}
                     
               if(CSaleBasket::Update(
                    $idasket,
                    $arData
                    )){
                    
                    $arResult = array(
                        'result' => 'ok',
                    );
                    
                }else{
                    $arResult = array(
                        'result' => 'error',
                        'data' => 'error update'
                    );
                }          
                                   }else{
                                        $arResult = array(
                                            'result' => 'error',
                                            'data' => 'error connection iblock'
                                        );
                                   }
                }else{
                    $arResult = array(
                        'result' => 'error',
                        'data' => 'error connection highloadblock'
                    );
                }
           
           }else{
                $arResult = array('result' => 'incorrect',
                                    'error' => 'Invalid token');
           }
           
            }else{
                $arResult = array(
                        'result' => 'incorrect'
                    );
            }
        }else{
            $arResult = array(
                        'result' => 'error',
                        'data' => 'error connection sale'
                    );
        }
        
        Response::ShowResult($arResult);
    }
    
    /**
     * ДОБАВЛЕНИЕ ТОВАРА В КОРЗИНУ
     * @param
     *      $uses - ID пользователя  
     *      $token - токен пользователя
     *      $product - ID продукта
     *      $color - название цвета
     *      $size - размер одежды
     *      $shoesizes - размер обуви
     *      $count - количество товара
     *      $delay - отложенный товар или нет true || false
     * @return
     *      $arResult = array(
     *          result => ok || error,
     *          data - код товара в корзине
     *      )
     */
     function addbasket(){
        if(CModule::IncludeModule("sale")){
            $json = file_get_contents('php://input');
            if( isset($json) ){
                $data = json_decode($json);
                $user = htmlspecialchars($data->user);
                $product = htmlspecialchars($data->product);
                $color = htmlspecialchars($data->color);
                $size = htmlspecialchars($data->size);
                $shoesizes = htmlspecialchars($data->shoesizes);
                $count = htmlspecialchars($data->count);
                $delay = htmlspecialchars($data->delay);
                
                $token=htmlspecialchars($data->token);
                if ($this->autorize_token($user,$token)){
                
                $arFUser = CSaleUser::GetList(array('USER_ID' => $user));//получаем код пользователя (для корзины))
                    if(!$arFUser['ID']){//если нет кода для данного пользователя
                         $int = CSaleUser::_Add(array('USER_ID' => $user));
                    }
                
                if($arFUser = CSaleUser::GetList(array('USER_ID' => $user))){//получаем код пользователя (для корзины))
                    
                    //получаем значение символьного кода переданного цвета
                    if(CModule::IncludeModule("highloadblock")){
                        $hlblock = HighloadBlockTable::getById(3)->fetch();   
                        $entity = HighloadBlockTable::compileEntity($hlblock);
                        $entity_data_class = $entity->getDataClass();
                        $rsData = $entity_data_class::getList(array(
                            'select' => array('UF_XML_ID','UF_RGB','UF_NAME')
                        ));
                        while($el = $rsData->fetch()){
                            if($el['UF_NAME']==$color)
                                $arColor = $el['UF_XML_ID'];//получили значение
                        }
                                            
                    //получаем id торгового предложения с переданноми свойствами
                    if(CModule::IncludeModule("iblock")){
                        $res = CIBlockElement::GetList(
                            array(),
                            array(
                                'IBLOCK_ID'                => 5, 
                                'ACTIVE'                   => 'Y',
                                'PROPERTY_CML2_LINK' => $product,
                                'PROPERTY_COLOR_REF' => $arColor,
                                'PROPERTY_SIZES_SHOES_VALUE' => $shoesizes,
                                'PROPERTY_SIZES_CLOTHES_VALUE' => $size
                            ),
                            false,
                            false,
                            array('ID'));
               
                        $ob = $res->GetNextElement();
                        $trade_ofer = CCatalogSKU::IsExistOffers($product,4);//есть ли у товара торговые предложения
                        if($ob || !$trade_ofer){//если мы получили торговое предложение или если у товара вообще нет торг. предложений
                            
                            if($trade_ofer){
                                $arFields = $ob->GetFields();
                                $idpreplog = $arFields['ID'];//ID торгового предложения для данного товара с переданными свойствами
                                $price = CPrice::GetBasePrice($idpreplog)['PRICE'];//получаем цену
                            }else{
                                $price = CPrice::GetBasePrice($product)['PRICE'];//получаем цену
                            }
                            
                            
                            //название товара
                            $resname = CIBlockElement::GetList(
                                array(),
                                array(
                                    'IBLOCK_ID' => 4, 
                                    'ID' => $product
                                ),
                                false,
                                false,
                                array('NAME','DETAIL_PAGE_URL'));
                            $obname = $resname->GetNextElement();
                            $arObname = $obname->GetFields();
                            $name = $arObname['NAME'];
                            $DETAIL_PAGE_URL = $arObname['DETAIL_PAGE_URL'];
                            
                            $Props = array();                            
                            if(!empty($color)) $Props[] = array( "NAME" => 'Цвет', 'CODE' => 'COLOR_REF' , 'VALUE' => $color );
                            if(!empty($size)) $Props[] = array( "NAME" => 'Размеры одежды ', 'CODE' => 'SIZES_CLOTHES' , 'VALUE' => $size );
                            if(!empty($shoesizes)) $Props[] = array( "NAME" => 'Размеры обуви', 'CODE' => 'SIZES_SHOES' , 'VALUE' => $shoesizes );
                            
                            if($trade_ofer){
                                $PRODUCT_XML_ID = $product.'#'.$idpreplog;
                            }else{
                                $PRODUCT_XML_ID = $product;
                                $idpreplog = $product;
                            }
                            
                            if($delay == true){
                                $delay = 'Y';
                            }else{
                                $delay = "N";
                            }
                            
                            if($result = CSaleBasket::Add(array(
                                            'MODULE' => 'catalog',
                                            'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
                                            'PRODUCT_ID' => $idpreplog, 
                                            'FUSER_ID' => $arFUser['ID'], 
                                            'QUANTITY' => $count, 
                                            'PRICE' => $price, 
                                            'CURRENCY' => 'RUB', 
                                            'LID' => 's1',
                                            'NAME' => $name,
                                            'PRODUCT_XML_ID' => $PRODUCT_XML_ID,
                                            'DETAIL_PAGE_URL' => $DETAIL_PAGE_URL,
                                            'DELAY' => $delay,
                                            'PROPS' => $Props))){
                                
                                $arResult = array(
                                    'result' => 'ok',
                                    'data' => $result
                                );
                                
                            }else{
                                $arResult = array(
                                    'result' => 'error',
                                    'data' => 'error Add to cart'
                                );
                            }
                        }else{
                            $arResult = array(
                                'result' => 'error',
                                'data' => 'error Getting the offer id'
                            );
                        }
                    }else{
                        $arResult = array(
                                'result' => 'error',
                                'data' => 'error connection iblock'
                            );
                    }
                    }else{
                        $arResult = array(
                                'result' => 'error',
                                'data' => 'error connection highloadblock'
                            );
                    }
                }else{
                    $arResult = array(
                        'result' => 'incorrect',
                        'data' => 'Error getting access code'
                    );
                }
            
            }else{
                $arResult = array('result' => 'incorrect',
                                    'error' => 'Invalid token');
            }
            
            }else{
                $arResult = array(
                        'result' => 'incorrect'
                    );
            }
        }else{
            $arResult = array(
                        'result' => 'error',
                        'data' => 'error connection sale'
                    );
        }
        
        Response::ShowResult($arResult);
     }
    
      /**
        * ПОЛУЧЕНИЕ АДРЕСА ДОСТАВКИ
        * @param
        *       $user - ID пользователя
        *       $token - токен пользователя
        * @return
        *       $arResult = array(
        *           resulr => ok || error,
        *           data => array(
        *               country - страна
        *               state - область 
        *               city - город
        *               zip - индекс
        *               street - улица
        *               apartment - дом, квартира
        *           )
        *       )
        */
     function get_adress(){
        
        $json = file_get_contents('php://input');
        if(isset($json)){
            $data = json_decode($json);
            $user = htmlspecialchars($data->user);
            
            $token=htmlspecialchars($data->token);
            if ($this->autorize_token($user,$token)){
            
             if(!empty($user)){
                
                $arUsers = CUser::GetList(($by="personal_country"),($order="desc"),array('ID' => $user, 'ACTIVE' => 'Y'));
                if($arUser = $arUsers->GetNext()){
                    
                    $country = $arUser['PERSONAL_COUNTRY'];
                    $state = $arUser['PERSONAL_STATE']; 
                    $city = $arUser['PERSONAL_CITY'];  
                    $zip = $arUser['PERSONAL_ZIP'];
                    $street = $arUser['PERSONAL_STREET'];
                    $apartment = $arUser['PERSONAL_MAILBOX'];
                                                            
                    $country = GetCountryByID($country);
                    
                    $result = array('country' => $country, 
                                    'state' => $state, 
                                    'city' => $city, 
                                    'zip' => $zip, 
                                    'street' => $street, 
                                    'apartment' => $apartment
                                    );
                    
                    $arResult = array(
                        'result' => 'ok',
                        'data' => $result
                    );
                    
                }else{
                    $arResult = array(
                        'result' => 'incorrect',
                        'error' => 'User is not found'
                    );
                }
             }else{
                $arResult = array(
                        'result' => 'incorrect',
                        'error' => 'No user id'
                    );
            }
        
        }else{
            $arResult = array('result' => 'incorrect',
                                    'error' => 'Invalid token');
        }
        
        }else{
            $arResult = array(
                        'result' => 'incorrect'
                    );
        }
        
        Response::ShowResult($arResult);
     }
     
     /**
      * ПОЛУЧИТЬ СПИСОК СТРАН И РЕГИОНОВ
      * @return
      *     $arResult = array(
      *         result => ok,
      *         data => array(
      *             country - массив стран
      *             state - массив областей
      *         )
      *     )
      */
      function get_locations(){
        
        $country = GetCountryArray();
        $country = $country['reference'];
        
        function getRegions() {
            $result = array();
            
            if (CModule::IncludeModule("sale")) {
                $db_vars = CSaleLocation::GetList(
                                array(
                            "REGION_NAME" => "ASC"
                                ), array("LID" => LANGUAGE_ID), false, false, array()
                );
                while ($vars = $db_vars->Fetch()) {
                    
                    if ($vars['REGION_ID'] && !$vars['CITY_ID']) {
                        $result[] =  $vars["REGION_NAME"];             
                    }
                }
            }
            return $result;
        }
        
        $state = getRegions();
        
        $arResult = array('result' => 'ok',
                        'data' => array(
                                    'country' => $country,
                                    'state' => $state
                                    ));
        
        Response::ShowResult($arResult);
      }
      
     //работа с контентом ------------------------------------------------------------------------------------------------------------------------------------------
     /**
      * ПОЛУЧИТЬ СПИСОК НОВОСТЕЙ
      * @return
      *     $arResult = array(
      *         result => ok || error,
      *         data => array(
      *             id - ID статьи
      *             title - название статьи
      *             image - изображение к статье
      *             text - краткое описание статьи
      *             date - дата публиуации
      *             clickable - имееться ли подробное описание (true || false)
      *         )
      *     )
      */
      function getnews(){
            
            if(CModule::IncludeModule("iblock")){
                $res = CIBlockElement::GetList(
                            array('active_from' => 'desc'),
                            array(
                                'IBLOCK_ID' => 1,
                                'ACTIVE' => 'Y',
                                'ACTIVE_DATE' => 'Y'
                            ),
                            false,
                            false,
                            array('ID','NAME','PREVIEW_PICTURE','PREVIEW_TEXT','DETAIL_TEXT'));
                 
                 $result=array();           
                 while($ob = $res->GetNextElement())
                {
                    $arFields = $ob->GetFields();
                    
                    $text = strip_tags($arFields['~PREVIEW_TEXT']);
                    $text = str_replace('&nbsp;',' ',$text);
                    $text = preg_replace('/\p{Cc}+/u','
',$text);
                    
                    if(empty($arFields['DETAIL_TEXT'])){
                        $clickable = false;
                    }else{
                        $clickable = true;
                    }
                    
                    $result[] = array(
                                    'id' => $arFields['ID'],
                                    'title' => $arFields['NAME'],
                                    'image' => $arFields['PREVIEW_PICTURE'] ? $this->HTTP.$_SERVER['SERVER_NAME'].CFile::GetPath($arFields['PREVIEW_PICTURE']) : null,
                                    'text' => $text,
                                    'date' => $arFields['ACTIVE_FROM'],
                                    'clickable' => $clickable
                                    );
                }
                
                $arResult = array('result' => 'ok',
                                    'data' => $result);
                
            }else{
                $arResult = array('result' => 'error',
                                    'error' => 'error connect iblock');
            }
            
            Response::ShowResult($arResult);
      }
      
      /**
       * ПОЛУЧИТЬ НОВОСТЬ ДЕТАЛЬНО
       * @param
       *    $id - ID новости
       * @return
       *    $arResult = array(
       *        result => ok || error,
       *        data => array(
       *            id - ID новости
       *            title - название
       *            image - подробная картинка
       *            text - подробное описание
       *        )
       *    )
       */
       function getnews_detail(){
            
            $json = file_get_contents('php://input');
            if(isset($json)){
                $data = json_decode($json);
                $id = htmlspecialchars($data->id);
                
                if(!empty($id)){
                    if(CModule::IncludeModule("iblock")){
                        $res = CIBlockElement::GetList(
                                    array('active_from' => 'desc'),
                                    array(
                                        'IBLOCK_ID' => 1,
                                        'ID' => $id,
                                        'ACTIVE' => 'Y',
                                        'ACTIVE_DATE' => 'Y'
                                    ),
                                    false,
                                    false,
                                    array('ID','NAME','DETAIL_PICTURE','DETAIL_TEXT'));
                         
                         if($ob = $res->GetNextElement()){
                            $arFields = $ob->GetFields();
                            $text = $arFields['DETAIL_TEXT'];
                            $text = str_replace('/upload/',$this->HTTP.$_SERVER['SERVER_NAME'].'/upload/',$text);
                            $text = preg_replace('/<a[^>]*>/','',$text);
                            $text = str_replace('</a>','',$text);
                          
                            $result = array('id' => $arFields['ID'],
                                            'title' => $arFields['NAME'],
                                            'image' => $arFields['DETAIL_PICTURE'] ? $this->HTTP.$_SERVER['SERVER_NAME'].CFile::GetPath($arFields['DETAIL_PICTURE']) : null,
                                            'text' => $text
                                            );
                            
                            $arResult = array('result' => 'ok',
                                        'data' => $result);
                         }else{
                            $arResult = array('result' => 'incorrect',
                                        'error' => 'New with the specified ID not found');
                         }
                        }else{
                             $arResult = array('result' => 'error',
                                        'error' => 'error connect iblock');
                        }
                }else{
                    $arResult = array('result' => 'incorrect',
                                        'error' => 'Not Specified News ID');
                }
            }else{
                $arResult = array('result' => 'error');
            }
            
            Response::ShowResult($arResult);
       }
           
}